export const ListaDenotas = [
  {
    idStudent: 1,
    notas: [10,15,12]
  },
  {
    idStudent: 2,
    notas: [15,15,12]
  },
  {
    idStudent: 3,
    notas: [5,8,20]
  },
  {
    idStudent: 4,
    notas: [11,15,18]
  },
  {
    idStudent: 5,
    notas: [20,18,19]
  },
  {
    idStudent: 6,
    notas: [8,9,11]
  },
  {
    idStudent: 7,
    notas: [10,18,9]
  },
  {
    idStudent: 8,
    notas: [10,18,9]
  },
  {
    idStudent: 9,
    notas: [10,18,9]
  },
  {
    idStudent: 10,
    notas: [10,18,9]
  },
];

export const AlumnosConNotasYpromedios = (listaStudiantes)=> {
  return listaStudiantes.map((alumno)=>{
    const notaDelAlumno = ListaDenotas.find((item)=>item.idStudent == alumno.id);
    const sumaDeNotas = notaDelAlumno.notas.reduce((total, item)=>{return (total+item);},0);
    return {alumno, notaDelAlumno, "promedio": Math.floor(sumaDeNotas/3)};
  });
};
