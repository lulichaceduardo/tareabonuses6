import estudiante from './estudiante';
//Condiserar un nombre descriptivo de lo que estamos importando.

const estudiante1 = new estudiante(1, 'Randy');
const estudiante2 = new estudiante(2, 'Ariadna');
const estudiante3 = new estudiante(3, 'Paul');
const estudiante4 = new estudiante(4, 'Mariana');
const estudiante5 = new estudiante(5, 'Frank');
const estudiante6 = new estudiante(6, 'Miguel');

//Exportacion por Nombre
export const listaStudiantes = [estudiante1, estudiante2, estudiante3, estudiante4, estudiante5, estudiante6];


